import pandas as pd


def load_data(val_size: float = 0.1, test_size: float = 0.1):
    """
    Get training, validation and test datasets.

    :param val_size: validation set size (in [0, 1])
    :param test_size: test set size (in [0, 1])
    :return: tuple with train, validation and test datasets as csv rows
    """
    assert 0 <= val_size <= 1, f"Validation set size must be between 0 and 1. Got {val_size}"
    assert 0 <= test_size <= 1, f"Test set size must be between 0 and 1. Got {test_size}"
    assert 0 <= val_size + test_size < 1, \
        f"0 <= val + test < 1 does not hold. Please correct validation and test set sizes."

    #whole_dataset = pd.read_csv("Abilene_tm.csv")
    whole_dataset = pd.read_csv("Linkloads.csv")
    train_count = len(whole_dataset) * (1 - (val_size + test_size))
    train = whole_dataset.loc[0:train_count - 1]

    val_count = len(whole_dataset) * val_size
    val = whole_dataset.loc[train_count:train_count + val_count - 1]

    test = whole_dataset.loc[train_count + val_count:]

    return train, val, test
