import time
import numpy as np

import torch
from torch import nn
from torch import optim
from torch import autograd

from util import load_data

import wandb
wandb.init(project="rnn-test", entity="mmhroth")

# Parse input data and put into torch tensors.
train_df, val_df, test_df = load_data(0.2, 0.2)
# NOTE: dropping the date for now!
train_df = train_df.drop(['date'], axis=1)
train_dataset = torch.tensor(train_df.to_numpy())
train_dataset = train_dataset.float()
val_df = val_df.drop(['date'], axis=1)
val_dataset = torch.tensor(val_df.to_numpy())
val_dataset = val_dataset.float()
test_df = test_df.drop(['date'], axis=1)
test_dataset = torch.tensor(test_df.to_numpy())
test_dataset = test_dataset.float()

# WandB configs.
wandb.config = {
  "learning_rate": 0.0001,
  "epochs": 33,
  "batch_size": 20
}

# TODO: Parameters.
class Parameters():
    """ Container for parameters. """
    def __init__(self, seq_len, batch_size, eval_batch_size, epochs,
                 log_interval, clip):
        self.seq_len = seq_len
        self.batch_size = batch_size
        self.eval_batch_size = eval_batch_size
        self.epochs = epochs
        self.log_interval = log_interval
        self.clip = clip

args = Parameters(seq_len=20, batch_size=wandb.config["batch_size"], eval_batch_size=wandb.config["batch_size"], epochs=wandb.config["epochs"], log_interval=10, clip=0.1)

# Check for device.
if torch.cuda.is_available():
    device = torch.device("cuda")
    print("Using GPU with CUDA.")
else:
    device = torch.device("cpu")
    print("Using CPU.")

class RNNPredictor(nn.Module):
    """Container module with an encoder, a recurrent module, and a decoder."""

    def __init__(self, encode_input_size, rnn_input_size,
                 rnn_hidden_size, decode_output_size, nlayers, dropout=0.5,
                 tie_weights=False, result_connection=False):
        super(RNNPredictor, self).__init__()
        self.encode_input_size = encode_input_size
        self.rnn_hidden_size = rnn_hidden_size
        self.nlayers = nlayers
        self.dropout = nn.Dropout(dropout)
        self.encoder = nn.Linear(encode_input_size, rnn_input_size)
        # Set RNN model, for now: ReLU
        self.rnn = nn.RNN(rnn_input_size, rnn_hidden_size, nlayers,
                          nonlinearity='relu', dropout=dropout)
        self.decoder = nn.Linear(rnn_hidden_size, decode_output_size)
        # Initialize weights.
        self.init_weights(initial_range=0.1)

    def init_weights(self, initial_range):
        self.encoder.weight.data.uniform_(-initial_range, initial_range)
        self.decoder.bias.data.fill_(0)
        self.decoder.weight.data.uniform_(-initial_range, initial_range)

    def init_hidden(self, batch_size):
        weight = next(self.parameters()).data
        #return autograd.Variable(weight.new(self.nlayers, batch_size, self.rnn_hidden_size).zero_())
        return autograd.Variable(weight.new(self.nlayers, self.rnn_hidden_size).zero_())

    def repackage_hidden(self, hidden):
        # Wrap hidden states in new Variables: detached them from their history.
        if type(hidden) == tuple:
            return tuple(self.repackage_hidden(val) for val in hidden)
        else:
            return hidden.detach()

    def forward(self, input, hidden, return_hiddens=False, noise=False):
        # TODO: check this.
        input = input.float()
        #emb = self.dropout(self.encoder(input.contiguous().view(self.encode_input_size)))
        #emb = emb.view(input.size(1), self.rnn_hidden_size)
        #emb = self.layerNorm1(emb)
        # RNN forward pass.
        input = self.encoder(input)
        output, hidden = self.rnn(input, hidden)
        output = output.contiguous().view(-1, self.rnn_hidden_size)
        decoded = self.decoder(output)
        #output = self.layerNorm2(output)
        #output = self.dropout(output)
        #decoded = self.decoder(output.view(output.size(0)*output.size(1), output.size(2)))
        #decoded = decoded.view(output.size(0), output.size(1), decoded.size(1))
        #if self.result_connection:
        #    decoded = decoded + input
        if return_hiddens:
            return decoded, hidden, output
        return decoded, hidden

# Build model.
model = RNNPredictor(encode_input_size=30,
                     rnn_input_size=64,
                     rnn_hidden_size=64,
                     decode_output_size=30,
                     nlayers=2,
                     dropout=0.0).to(device)
optimizer = optim.Adam(model.parameters(), lr=wandb.config["learning_rate"], weight_decay=1e-4)
criterion = nn.MSELoss()

# Split into batches.
def get_batch(args, input_data, idx):
    direct_seq_len = min(args.seq_len, len(input_data) - 1 - idx)
    data = input_data[idx : idx + direct_seq_len]
    target = input_data[idx + 1 : idx + 1 + direct_seq_len]
    return data, target

#print(train_dataset.size(0))
#print(train_dataset[1])
#print("NOW the Batches.")
#input_seq, target_seq = get_batch(args, train_dataset, 0)
#print(input_seq.shape)
#print(target_seq)

# Train model.
def train(args, model, train_dataset, epoch):
    with torch.enable_grad():
        loss_sum = 0
        counter = 0
        # Turn on training mode which enables dropout.
        model.train()
        total_loss = 0
        start_time = time.time()
        hidden = model.init_hidden(args.batch_size)
        # TODO: checkf dimensions!
        for batch_idx, i in enumerate(range(0, train_dataset.size(0) - 1, args.seq_len)):
            input_seq, target_seq = get_batch(args, train_dataset, i)
            # Starting each batch, we detach the hidden state from how it was previously produced.
            # If we didn't, the model would try backpropagating all the way to start of the dataset.
            hidden = model.repackage_hidden(hidden)
            optimizer.zero_grad()
            # Forward pass for loss.
            output_seq, hidden = model.forward(input_seq, hidden, return_hiddens=False)
            # Calculate loss here.
            #loss = criterion(output_seq.view(args.batch_size, 1), target_seq.view(args.batch_size, 1))
            loss = criterion(output_seq, target_seq)
            # Backpropagate.
            #loss = loss.float()
            loss.backward()
            # Prevent the exploding gradient problem in RNNs / LSTMs.
            # TODO: verify if needed.
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)
            optimizer.step()
            total_loss += loss.item()
            #wandb.watch(model)
            # Show and reset total loss & timinig for batch.
            if batch_idx % args.log_interval == 0 and batch_idx > 0:
                current_loss = total_loss / args.log_interval
                loss_sum += current_loss
                counter += 1
                #wandb.log({"current_loss": current_loss})
                elapsed = time.time() - start_time
                #print('| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.4f} | '
                #      'loss {:5.2f} '.format(
                #          epoch, batch_idx, len(train_dataset) // args.seq_len,
                #          elapsed * 1000 / args.log_interval, current_loss))
                total_loss = 0
                start_time = time.time()
        return loss_sum / counter

# Evaluate model.
def evaluate(args, model, test_dataset):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    with torch.no_grad():
        total_loss = 0
        hidden = model.init_hidden(args.eval_batch_size)
        nbatch = 1
        for batch_idx, idx in enumerate(range(0, test_dataset.size(0) - 1, args.seq_len)):
            input_seq, target_seq = get_batch(args, test_dataset, idx)
            hidden_ = model.repackage_hidden(hidden)
            # Forward pass for loss.
            output_seq, hidden = model.forward(input_seq, hidden, return_hiddens=False)
            # Calculate loss here.
            #loss = criterion(output_seq.view(args.batch_size, -1), target_seq.view(args.batch_size, -1))
            loss = criterion(output_seq, target_seq)
            total_loss += loss.item()
    return total_loss / (batch_idx + 1)

# Looping over epochs.
best_val_loss = float('inf')
print("Starting training now.")
try:
    for epoch in range(1, args.epochs + 1):
        epoch_start_time = time.time()
        train_loss = train(args, model, train_dataset, epoch)
        wandb.log({"Training loss": train_loss})
        val_loss = evaluate(args, model, test_dataset)
        wandb.log({"Validation loss": val_loss})
        print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.4f} | '
              .format(epoch, (time.time() - epoch_start_time), val_loss))
except KeyboardInterrupt:
    print("Interrupted.")
print("Finished.")
