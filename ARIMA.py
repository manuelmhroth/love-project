from util import load_data
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.tsa.ar_model import AutoReg
from statsmodels.tsa.arima_process import ArmaProcess
from statsmodels.stats.diagnostic import acorr_ljungbox
from statsmodels.tsa.statespace.sarimax import SARIMAX
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import pacf
from statsmodels.tsa.stattools import acf
from tqdm import tqdm_notebook
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from itertools import product

class ARMA:
    def __init__(self, train, val):
        self.train = train
        self.val = val
        self.model = None

    def stationarity(self):
        #
        # Check for stationarity of the time-series data
        # We will look for p-value. In case, p-value is less than 0.05, the time series
        # data can said to have stationarity
        #
        #
        # Run the test
        #
        df_stationarityTest = adfuller(self.train, autolag='AIC')
        #
        # Check the value of p-value
        #
        print(f"Is P-value below 0.05? {df_stationarityTest[1]<0.05}")
        print("P-value: ", df_stationarityTest[1])

    def pacf(self, lags = 1000):
        #
        # Next step is to find the order of AR model to be trained
        # for this, we will plot partial autocorrelation plot to assess
        # the direct effect of past data on future data
        #
        pacf = plot_pacf(self.train, lags=100, method='ywm')
        plt.show()

    def acf(self, lags = 100):
        acf = plot_acf(self.train, lags=100)
        plt.show()

    def fit(self):
        self.model = ARIMA(self.train, order=(50,0,50)).fit()

    def predict(self, start=None, end=None, dynamic=True):
        #
        # Make the predictions
        #
        if not start:
            start = len(self.train)
        if not end:
            end = (self.train + self.val)
        pred = self.model.predict(start=start, end=end, dynamic=dynamic)
        #
        # Plot the prediction vs test data
        #
        plt.plot(pred, color='red')
        plt.plot(val)
        plt.show()

    def optimize(self, order_list, exog):
        """
            Return dataframe with parameters and corresponding AIC

            order_list - list with (p, d, q) tuples
            exog - the exogenous variable
        """

        results = []

        for order in order_list:
            try: 
                model = SARIMAX(exog, order=order).fit(disp=-1)
            except:
                continue

            aic = model.aic
            results.append([order, model.aic])

        result_df = pd.DataFrame(results)
        result_df.columns = ['(p, d, q)', 'AIC']
        #Sort in ascending order, lower AIC is better
        result_df = result_df.sort_values(by='AIC', ascending=True).reset_index(drop=True)

        return result_df
    
    def opt(self, ps=range(0,290,10), d=[0], qs = range(0,290,10)):
        # Create a list with all possible combination of parameters
        parameters = product(ps,d, qs)
        parameters_list = list(parameters)
        
        order_list = parameters_list

        result_df = self.optimize(order_list, exog=self.train)
        return result_df
    
    def best_model(self):
        self.model = SARIMAX(self.train, order=self.opt()[0]).fit()
        print(self.model.summary())
