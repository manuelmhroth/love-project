import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.tsa.ar_model import AutoReg

class AR:
    def __init__(self,train,val,set):
        self.train = train
        self.val = val
        self.set = set
        self.model = None


    def stationarity(self):
        df_stationarityTest = adfuller(self.train, autolag='AIC')
        #
        # Check the value of p-value
        #
        print(f"Is P-value below 0.05? {df_stationarityTest[1]<0.05}")
        print("P-value: ", df_stationarityTest[1])


    def pacf(self, lags=100):
        #
        # Next step is to find the order of AR model to be trained
        # for this, we will plot partial autocorrelation plot to assess
        # the direct effect of past data on future data
        #
        pacf = plot_pacf(self.train, lags=lags, method='ywm')

        plt.savefig(f"plots/train_pacf_{self.set}.png")


    def fit(self, lags=1000):
        #
        # Instantiate and fit the AR model with training data
        #
        self.model = AutoReg(self.train, lags=lags).fit()
        # ar_select_order(housing, 13, seasonal=True, old_names=False)
        time = datetime.datetime.now().strftime('%d_%H:%M:%S')
        dest = f"./models/AR/model_{time}"
        self.model.save(dest)
        #
        # Print Summary
        #
        print(self.model.summary())




    def predict(self, start=None, end=None, dynamic=False):
        #
        # Make the predictions
        #
        if not start:
            start = len(self.train)
        if not end:
            end = len(self.train + self.val)

        pred = self.model.predict(start=start, end=end, dynamic=dynamic)

        
        fig = self.model.plot_predict(start, end, dynamic)
        plt.plot(self.val[:50])
        plt.savefig(f"./plots/AR/plot_predict_{self.set}.png")
        fig.clear()
        
        fig = plt.figure(figsize=(16, 9))
        fig = self.model.plot_diagnostics(fig=fig, lags=30)
        plt.savefig(f"./plots/AR/plot_diagnostics_{self.set}.png")
        fig.clear()

        #
        # Plot the prediction vs test data
        #
        plt.plot(self.train[38500:])
        plt.plot(self.val[:50])
        plt.plot(pred, color='red')
        plt.savefig(f"./plots/AR/Prediction_on_Validation_set_{self.set}.png")
