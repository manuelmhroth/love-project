from util import load_data
import AR
import ARIMA
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def AR_model(train, val, set):
    ar = AR.AR(train, val, set)
    ar.stationarity()
    ar.pacf(lags=100)
    ar.fit(lags=100)
    ar.predict(start=(len(train)), end=(len(train) + 50), dynamic=True)

def ARIMA_model(train, val, test):
    arima = ARIMA.ARIMA(train, val)
    arima.stationarity()
    arima.pacf(lags=100)
    arima.acf(lags=100)
    arima.fit(lags=100)
    arima.predict(start=(len(train)), end=(len(val)), dynamic=True)

if __name__ == '__main__':
    train, val, test = load_data()
    # print(train)
    # print(val)
    # print(test)
    
    # df = pd.read_csv('./Linkloads.csv',sep=",")
    # df = pd.read_csv('./Abilene_tm.csv',sep=",")
    # #df['1'][:2000].plot()
    # #plt.show()
    # train = df['1'][:len(df)-2000]
    # val = df['1'][(len(df)-3000):(len(df)-1900)]
    # test = df['1'][len(df)-1000:]
    for i in range(1,30,1):
        train_d = train[str(i)]
        val_d = val[str(i)]
        AR_model(train_d, val_d, i)